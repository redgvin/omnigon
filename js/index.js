(function(){
	function Widget(options) {
		this.options = Object.assign({
			container: '#container', //id or class
			feedUrl: 'http://api.massrelevance.com/MassRelDemo/kindle.json',
			postsNumber: 5,
			updateInterval: 5 // seconds
		}, options);

		// only for showing that posts updated
		this.updateCounter = 0;

		setTimeout(function run() {
		  this.getPosts(this.update.bind(this));
		  setTimeout(run.bind(this), this.options.updateInterval * 1000);
		}.bind(this), 0);
	}

	Widget.prototype.update = function (response) {
		this.updateCounter ++;
		response = JSON.parse(response);
		response = response.slice(0, this.options.postsNumber);

		let container = document.querySelector(this.options.container);


		let html = document.createDocumentFragment();
		for (let i = 0, len = response.length; i < len; i++) {
			let date = new Date(response[i].created_at);
			let hours = date.getHours();
			let minutes = date.getMinutes();

			if (hours < 10) {
				hours = '0' + hours;
			}
			if (minutes < 10) {
				minutes = '0' + minutes;
			}

			date = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear() + ' ' + hours + ':' + minutes;
		 
		  	html.appendChild(this.renderPost({
				date: date,
				author: response[i].user.name,
				message: response[i].text
			}));
		}
		container.innerHTML = '';
		container.appendChild(html);
	}

	Widget.prototype.renderPost = function(data) {
		let post = document.createElement('div');
		let date = document.createElement('div');
		let author = document.createElement('div');
		let message = document.createElement('div');
		let dataText = document.createTextNode(data.date);
		let authorText = document.createTextNode(data.author);
		let messageText = document.createTextNode(data.message);
		
		date.appendChild(dataText);
		date.className = 'date';

		author.appendChild(authorText);
		author.className = 'author';

		message.appendChild(messageText);
		message.className = 'message';
		
		post.appendChild(message)
		post.appendChild(author)
		post.appendChild(date);

		post.className = 'post' + ((this.updateCounter % 2 == 0) ? ' post-blue': '');

		return post;
	}

	Widget.prototype.getPosts = function (callback) {
		let xhr = new XMLHttpRequest();
		let url = this.options.feedUrl;

		if (url.indexOf('limit') === -1) {
			url += ((url.indexOf('?') === -1) ? '?' : '&') + 'limit=' + this.options.postsNumber;
		}

		xhr.open('GET', url, true);

		xhr.send();

		xhr.onreadystatechange = function() {
		  if (this.readyState != 4) return;


		  if (this.status != 200) {
		  	console.log('error: ' + (this.status ? this.statusText : 'bad request'))
		    return;
		  }


		  callback(this.responseText);
		}
	}

	new Widget();
	new Widget({
		container: '#container2',
		postsNumber: 10,
		updateInterval: 7
	});
})()
